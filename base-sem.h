
#ifndef __SEM_COMMON__
#define __SEM_COMMON__

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/sem.h>

#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif 


extern int create_sem(int key_id, int count);

extern int attach_sem(int key_id, int count);

extern int free_sem(int semid);

extern int set_sem_value(int semid, int index, int value);

extern int get_sem_value(int semid, int index);

extern int try_lock_sem(int semid, int index, int ms);

extern int lock_sem(int semid, int index);

extern int unlock_sem(int semid, int index);


#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif 

#endif  
