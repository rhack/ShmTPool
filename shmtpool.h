#ifndef __SHM_TPOOL__
#define __SHM_TPOOL__
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <sys/mman.h>	// Shared memory
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

	   
#ifndef __cplusplus
#ifndef bool
typedef char bool;
#endif

#ifndef true
#define true	((bool) 1)
#endif

#ifndef false
#define false	((bool) 0)
#endif

#endif   /* not C++ */
#endif 

#define SHM_BUFF_LEN 1024 //1M
#define SHM_KEY 0x1235
#define KEY_ID  11

#define IDLE_STATUS   0
#define BUSY_STATUS   1
#define UNKNEW_STATUS 2

typedef struct __process_id
{
	int pid;
	int index;
}process_id;

typedef struct __task_st
{
	unsigned short index_pro;
	unsigned short init_success;
	unsigned short cur_task_status; //表示任务是否完成
	char data[SHM_BUFF_LEN + 1]; //单个进程的共享内存大小
}task_st;

typedef struct __shared_st
{
	unsigned short stop;//表示进程池的退出标志
	task_st   *task_pro; //每个子进程需要处理的任务块信息 
}shared_st;

typedef struct __process_pool
{
	int size;
	key_t shm_key; //新建共享内存的key信息
	int shmid;     //共享内存的id
	int semid;   //同步信号量的id标识
	pthread_t  manager_pthread_id;
	process_id *id; // fork的进程的pid信息  
	shared_st *shared_mem;  //共享内存的首地址

}process_pool;

typedef struct __st_data
{
   int slen;
   char data[SHM_BUFF_LEN - sizeof(int)];
}st_data;


/*
http://www.ibm.com/developerworks/cn/linux/l-ipc/part5/index2.html
http://www.cnblogs.com/biyeymyhjob/archive/2012/10/11/2720377.html
http://www.cnblogs.com/resound/archive/2011/07/26/2117243.html

http://my.oschina.net/faterer/blog/97526?p={{currentPage+1}}
http://blog.csdn.net/boxsoldier/article/details/6843795


*/
